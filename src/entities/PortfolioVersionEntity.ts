import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';

import PageEntity from './PageEntity';
import { PortfolioVersionTypeEnum } from '../enum/PorfolioVersionTypeEnum';
import PortfolioEntity from './PortfolioEntity';

@ObjectType('PortfolioVersion')
@Entity()
export default class PortfolioVersionEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => PortfolioVersionTypeEnum)
  @Column('varchar')
  type: PortfolioVersionTypeEnum;

  @OneToMany(() => PageEntity, (page) => page.portfolioVersion)
  pages: PageEntity[];

  @ManyToOne(() => PortfolioEntity)
  portfolio: PortfolioEntity;
}
