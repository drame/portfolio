import { Arg, Mutation, Resolver } from 'type-graphql';
import { Service } from 'typedi';
import { Repository, getRepository } from 'typeorm';

import PortfolioVersionEntity from '../entities/PortfolioVersionEntity';
import { PortfolioVersionTypeEnum } from '../enum/PorfolioVersionTypeEnum';
import { CreatePortfolioVersionInput } from '../input/CreatePortfolioVersionInput';

@Resolver()
@Service()
export default class PortfolioVersionResolver {
  public portfolioVersionRepository: Repository<PortfolioVersionEntity>;

  constructor() {
    this.portfolioVersionRepository = getRepository(PortfolioVersionEntity);
  }

  @Mutation(() => PortfolioVersionEntity, { description: 'Create snapshot version using draft' })
  async createSnapshotFromDraft(@Arg('porfolio') portfolio: string): Promise<PortfolioVersionEntity> {
    const portfolioDraftVersion = await this.portfolioVersionRepository.findOne({
      where: { type: PortfolioVersionTypeEnum.DRAFT, portfolio },
    });
    if (!portfolioDraftVersion) {
      throw Error('No draft version');
    }
    const res = this.portfolioVersionRepository.create({
      ...portfolioDraftVersion,
      type: PortfolioVersionTypeEnum.SNAPSHOT,
    });
    return this.portfolioVersionRepository.save(res);
  }

  @Mutation(() => PortfolioVersionEntity, { description: 'Create new portfolio version' })
  async createPortfolioVersion(@Arg('input') input: CreatePortfolioVersionInput): Promise<PortfolioVersionEntity> {
    const res = this.portfolioVersionRepository.create({ ...input, portfolio: { id: input.portfolioId } });
    return this.portfolioVersionRepository.save(res);
  }
}
