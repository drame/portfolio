import { Arg, Mutation, Query, Resolver } from 'type-graphql';
import { Service } from 'typedi';
import { Repository, getRepository } from 'typeorm';

import PortfolioEntity from '../entities/PortfolioEntity';
import { CreatePortfolioInput } from '../input/CreatePortfolioInput';

@Resolver()
@Service()
export default class ListPortfoliosResolver {
  public portfolioRepository: Repository<PortfolioEntity>;

  constructor() {
    this.portfolioRepository = getRepository(PortfolioEntity);
  }

  @Query(() => [PortfolioEntity], { description: 'List all portfolios' })
  async listPortfolios(): Promise<PortfolioEntity[]> {
    const res = await this.portfolioRepository.createQueryBuilder('p').getMany();
    return res;
  }

  @Mutation(() => PortfolioEntity, { description: 'Create new portfolio' })
  async createPortfolio(@Arg('input') portfolio: CreatePortfolioInput): Promise<PortfolioEntity> {
    const res = this.portfolioRepository.create(portfolio);
    return this.portfolioRepository.save(res);
  }
}
