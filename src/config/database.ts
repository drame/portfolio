import config from './index';

export const dbConfig = {
  ...config.database,
  synchronize: false,
};
