import { Field, InputType } from 'type-graphql';
import { PortfolioVersionTypeEnum } from '../enum/PorfolioVersionTypeEnum';

@InputType()
export class CreatePortfolioVersionInput {
  @Field(() => PortfolioVersionTypeEnum, { nullable: false })
  type: PortfolioVersionTypeEnum;

  @Field({ nullable: false })
  portfolioId: number;
}
