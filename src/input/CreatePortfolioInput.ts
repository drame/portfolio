import { Field, InputType } from 'type-graphql';

@InputType()
export class CreatePortfolioInput {
  @Field({ nullable: false })
  name: string;

  @Field({ nullable: false })
  url: string;
}
