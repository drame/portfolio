import { registerEnumType } from 'type-graphql';

export enum PortfolioVersionTypeEnum {
  DRAFT,
  SNAPSHOT,
  PUBLISHED,
}
registerEnumType(PortfolioVersionTypeEnum, { name: 'PortfolioVersionTypeEnum' });
